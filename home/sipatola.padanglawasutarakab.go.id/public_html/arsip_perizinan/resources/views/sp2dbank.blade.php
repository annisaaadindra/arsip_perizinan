@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">SP2D Masih di Proses</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Visualisasi Data</span> - SP2D Masih di Proses</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Monitoring SPM yang Sedang Dalam Proses Penerbitan SP2D</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>


					<table class="table table-striped datatable-responsive">
						<thead>
							<tr>
								<th>Nama Unit Organisasi</th>
								<th>Penerima</th>
								<th>Nomor SP2D</th>
								<th>Tgl SP2D</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Badan Pengelola Keuangan dan Aset</td>
								<td>Tes</td>
								<td>182/2018</td>
								<td>14 Februari 2018</td>
								<td>Rp <?php $angka = mt_rand(7000000, 30000000); echo number_format($angka,2,",",".")?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/pages/datatables_responsive.js"></script>

</body>
</html>
