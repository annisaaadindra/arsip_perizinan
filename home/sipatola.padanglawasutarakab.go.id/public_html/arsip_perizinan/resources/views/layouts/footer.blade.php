	<div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2019. <a href="#" class="navbar-link"><b>DPTSP</b> &#8212; ARSIP PERIZINAN</a> oleh <a href="http://themeforest.net/user/Kopyov" class="navbar-link" target="_blank">DPTSP  Pemerintah Kabupaten Padang Lawas Utara</a>
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#">Tentang</a></li>
					<li><a href="#">Kebijakan</a></li>
					<li><a href="#">Kontak</a></li>
				</ul>
			</div>
		</div>
	</div>