<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- Navigasi Utama -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-title h6">
							<span>Navigasi Utama</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content sidebar-user">
							<div class="media">
								<a href="#" class="media-left"><img src="<?= Config::get('global.base_url');?>assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Dev Mode</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;DINAS PTSP 
									</div>
								</div>

							</div>
						</div>

						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Utama</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="{{ url('beranda') }}"><i class="icon-home4"></i> <span>Beranda</span></a></li>
								<!-- /layout -->

								<!-- Data visualization -->
								<li class="navigation-header"><span>LAPORAN UMUM:</span> <i class="icon-menu" title="Data visualization"></i></li> 
								<li>
									<a href="{{ url('pendaftaran') }}"><i class="icon-graph"></i> <span>Pendaftaran Arsip Backoffice</span></a>
								</li>
								<li>
									<a href="{{ url('pendaftaranoss') }}"><i class="icon-graph"></i> <span>Pendaftaran Arsip OSS</span></a>
								</li>
								<li>
									<a href="{{ url('pengelolaan') }}"><i class="icon-graph"></i> <span>Pengelolaan Arsip Backoffice</span></a>
								</li> 
								<li>
									<a href="{{ url('pengelolaanoss') }}"><i class="icon-graph"></i> <span>Pengelolaan Arsip OSS</span></a>
								</li> 
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>