<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'LoginController@index');
Route::get('beranda', 'HomeController@index');
Route::get('pendaftaran', 'PendaftaranController@index');
Route::get('pendaftaranoss', 'PendaftaranossController@index');
Route::get('pengelolaan', 'PengelolaanController@index');
Route::get('pengelolaanoss', 'PengelolaanossController@index');
Route::get('pengelolaan/{pendaftaran_id}', 'PengelolaanController@detail');
Route::get('pengelolaanoss/{pendaftaran_id}', 'PengelolaanossController@detail');
Route::post('form/login', 'LoginController@login');
Route::post('form/pendataan', 'FormController@pendataan');
Route::post('form/pendataanoss', 'FormController@pendataanoss');
Route::post('form/pengecekan', 'FormController@pengecekan');
Route::get('cekberkas', 'CekBerkasController@index');
Route::get('cekberkas/{pendaftaran_id}', 'PengelolaanController@cekberkas');


Route::get('halo', function(){
	return 'Halo';
});

Route::get('contents', 'ContentController@index');