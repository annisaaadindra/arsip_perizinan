<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "tes";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendataan()
    {
        $no_daftar =  Input::get('no_pendaftaran');
        $value=DB::table('arsip_file')->where('pendaftaran_id','=',$no_daftar)->get();
        count($value);
        if (count($value) == '0')
        {  
            $tmpermohonan=DB::table('tmpermohonan')->where('pendaftaran_id','=',$no_daftar)->get();
                $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                foreach($files as $file) {
                  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                  $validator = Validator::make(array('file'=> $file), $rules);
                  if($validator->passes()){
                    $destinationPath = 'uploads';
                    $filename = $file->getClientOriginalName();
                    $upload_success = $file->move($destinationPath, $filename);
                    $uploadcount ++;
                    DB::insert('insert into arsip_file values ("'.$no_daftar.'","'.$filename.'")');

                  }
                }

            return Redirect::to('pendaftaran')->with('berhasil','Berhasil Ditambahkan');
            
        }
        else{
            return Redirect::to('pendaftaran')->with('gagal','Gagal Menambahkan. Nomor pendaftaran yang anda input sudah ada perah diarsipkan.');
             
        }

    }

    public function pendataanoss()
    {
        $no_daftar =  Input::get('no_pendaftaran');
        $nama =  Input::get('nama');
        $value=DB::table('oss_pendaftaran')->where('id','=',$no_daftar)->get();
        count($value);
        if (count($value) == '0')
        {  
            DB::insert('insert into oss_pendaftaran values ("'.$no_daftar.'","'.$nama.'",now())');
            echo 'insert into oss_pendaftaran values ("'.$no_daftar.'","'.$nama.'",now())';
                //$tmpermohonan=DB::table('tmpermohonan')->where('pendaftaran_id','=',$no_daftar)->get();
                $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                foreach($files as $file) {
                  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                  $validator = Validator::make(array('file'=> $file), $rules);
                  if($validator->passes()){
                    $destinationPath = 'uploads';
                    $filename = $file->getClientOriginalName();
                    $upload_success = $file->move($destinationPath, $filename);
                    $uploadcount ++;
                    DB::insert('insert into oss_arsip values ("'.$no_daftar.'","'.$filename.'")');

                  }
                }

            return Redirect::to('pendaftaranoss')->with('berhasil','Berhasil Ditambahkan');
            
        }
        else{
         return Redirect::to('pendaftaranoss')->with('gagal','Gagal Menambahkan. Nomor pendaftaran yang anda input sudah ada pernah diarsipkan.');
             
        }

    }

    public function pengecekan()
    {
        $no_daftar =  Input::get('pendaftaran_id');
        $value=DB::table('arsip_file')->where('pendaftaran_id','=',$no_daftar)->distinct()->get();
        count($value);
        var_dump($value);
        if(count($value) != '0'){
            return Redirect::to('cekberkas/'.$value[0]['pendaftaran_id']);
        }
        else{
            return Redirect::to('cekberkas')->with('gagal1','Nomor pendaftaran yang anda input tidak ada didalam sistem.');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
