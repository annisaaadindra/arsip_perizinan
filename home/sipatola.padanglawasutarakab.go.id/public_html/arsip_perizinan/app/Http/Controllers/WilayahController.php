<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//WAJIB PAKE INI UNTUK BISA PAKE [DB::]
use Illuminate\Support\Facades\DB;

class WilayahController extends Controller
{ 
    function index(){
    	$value=DB::table('provinces')->orderBy('name', 'asc')->get();
         //return $value;
    	return view('wilayah', ['provinces' => $value]);
    }

    function provchild($prov){
    	$value=DB::table('regencies')->where('province_id', $prov)->orderBy('name', 'asc')->get();
    //return $value;
    	return view('wilayah', ['provinces' => $value]);
    }

    function regchild($reg){
    	$value=DB::table('districts')->where('regency_id', $reg)->orderBy('name', 'asc')->get();
    //return $value;
    	return view('wilayah', ['provinces' => $value]);
    }

    function dischild($dis){
    	$value=DB::table('villages')->where('district_id', $dis)->orderBy('name', 'asc')->get();
    //return $value;
    	return view('wilayah', ['provinces' => $value]);
    }
}
