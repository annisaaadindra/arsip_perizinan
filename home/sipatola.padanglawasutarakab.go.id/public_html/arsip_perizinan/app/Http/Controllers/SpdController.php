<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SpdController extends Controller
{
    function index($triwulan){
    	$data['triwulan'] = $triwulan;
    	return view('spd'.$triwulan);
    }

}
