<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SppController extends Controller
{
    function index($status){
    	$data['status'] = $status;
    	return view('spp'.$status);
    }
}
