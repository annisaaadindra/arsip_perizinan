<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    function index(){
    	$data = Content::get();
    	return view(('tasks'), compact('data'));
    }
}
