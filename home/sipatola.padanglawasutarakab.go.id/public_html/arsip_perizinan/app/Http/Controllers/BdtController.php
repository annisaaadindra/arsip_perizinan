<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;


class BdtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bdt');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kksnonpkh()
    {
        return view('kksnonpkh');
    }

    public function kksnonpkh_detail($nik_kk)
    { 
        $value=DB::table('kks_non_pkh')->where('no_kk', $nik_kk)->orderBy('no_kk', 'desc')->get();
         //return $value;
        return view('kksnonpkh_detail', ['kirim' => $value, 'nik_kk' => $nik_kk]); 
    }

    public function kkspkh_detail($nik_kk)
    { 
        $value=DB::table('kks_pkh')->where('no_kk', $nik_kk)->orderBy('no_kk', 'desc')->get();
         //return $value;
        return view('kkspkh_detail', ['kirim' => $value, 'nik_kk' => $nik_kk]); 
    }

    public function bdtnonpkhkks()
    {
        return view('bdtnonpkhkks');
    }

    public function bdtbpjsnonkksnonpkhnonpbi()
    {
        return view('bdtbpjsnonkksnonpkhnonpbi');
    }

    public function kkspkh()
    {
        return view('kkspkh');
    }

    public function kkspbi_nonpkh()
    {
        return view('kkspbi_nonpkh');
    }

    //====

    public function bpjsnonpbi()
    {
        return view('bpjsnonpbi');
    }

    public function kksbpjsnonpkhnonpbi()
    {
        return view('kksbpjsnonpkhnonpbi');
    }

    public function bdtpbinonkkspkh()
    {
        return view('bdtpbinonkkspkh');
    }

    public function pkhkksbpjsnonpbi()
    {
        return view('pkhkksbpjsnonpbi');
    }

    public function usulanpbibpjs()
    {
        return view('usulanpbibpjs');
    }

    public function pbibpjsbdtnonpkhkks()
    {
        return view('pbibpjsbdtnonpkhkks');
    }

    public function pbibpjsbdtkksnonpkh()
    {
        return view('pbibpjsbdtkksnonpkh');
    }

    public function pbibpjsbdtkkspkh()
    {
        return view('pbibpjsbdtkkspkh');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
