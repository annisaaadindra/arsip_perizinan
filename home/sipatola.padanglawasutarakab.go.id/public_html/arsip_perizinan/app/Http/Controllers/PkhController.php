<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;


class PkhController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('pkh');
    }

    public function detail($nik_kk)
    { 
        $value=DB::table('pbi')->where('nik_kk', $nik_kk)->orderBy('nik_kk', 'desc')->get();
         //return $value;
        return view('pbidetail', ['kirim' => $value, 'nik_kk' => $nik_kk]); 
    }

}
