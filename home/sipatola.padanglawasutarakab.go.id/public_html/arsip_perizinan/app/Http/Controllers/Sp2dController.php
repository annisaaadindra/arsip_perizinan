<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Sp2dController extends Controller
{
    function index(){
    }
    
    function proses(){
    	return view('sp2dproses');
    }

    function selesai(){
    	return view('sp2dselesai');
    }

    function bank(){
    	return view('sp2dbank');
    }
}
