/* ------------------------------------------------------------------------------
 *
 *  # Google Visualization - bars
 *
 *  Google Visualization bar chart demonstration
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */


// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar);


// Chart settings
function drawBar() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Unit', 'Dianggarkan', 'Direalisasikan'],
        ['Disdik',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
        ['Disdik',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Dinkes',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['BPKA',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Bappeda',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Inspektorat',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Diskominfo',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disparbudpora',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DPMP2TSP',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Pertanian',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DLH',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Dispursip',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disdukcapil',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Diskanla',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disketapang',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disnakerop',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DISPMD',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Dishub',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disperindag',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Disperkim',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DISDPPKB3A',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DISPUPR',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['DistatpolPP',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Dinsos',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['BKD',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Bakesbangpol',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['BPBD',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)],
            ['Bapenda',Math.floor((Math.random() * 1000000000) + 100000000),Math.floor((Math.random() * 800000000) + 100000000)]
    ]);


    // Options
    var options_bar = {
        fontName: 'Roboto',
        height: 2050,
        fontSize: 12,
        chartArea: {
            left: '10%',
            width: '90%',
            height: 2000
        },
        tooltip: {
            textStyle: {
                fontName: 'Roboto',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'top',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar')[0]);
    bar.draw(data, options_bar);

}


// Resize chart
// ------------------------------

$(function () {

    // Resize chart on sidebar width change and window resize
    $(window).on('resize', resize);
    $(".sidebar-control").on('click', resize);

    // Resize function
    function resize() {
        drawBar();
    }
});
