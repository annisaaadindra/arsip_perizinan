<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class PengelolaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $value=DB::select('select DISTINCT a.pendaftaran_id, d.n_pemohon, f.n_perizinan, b.d_terima_berkas from arsip_file a inner join tmpermohonan b on a.pendaftaran_id = b.pendaftaran_id
inner join tmpemohon_tmpermohonan c on b.id = c.tmpermohonan_id 
inner join tmpemohon d on d.id = c.tmpemohon_id
inner join tmpermohonan_trperizinan e on e.tmpermohonan_id = b.id
inner join trperizinan f on f.id = e.trperizinan_id');
        return view('pengelolaan',['kirim' => $value]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($pendaftaran_id)
    {
        $value=DB::select('select DISTINCT a.pendaftaran_id, a.file, d.n_pemohon, f.n_perizinan, b.d_terima_berkas from arsip_file a inner join tmpermohonan b on a.pendaftaran_id = b.pendaftaran_id
inner join tmpemohon_tmpermohonan c on b.id = c.tmpermohonan_id 
inner join tmpemohon d on d.id = c.tmpemohon_id
inner join tmpermohonan_trperizinan e on e.tmpermohonan_id = b.id
inner join trperizinan f on f.id = e.trperizinan_id where a.pendaftaran_id = "'.$pendaftaran_id.'"');
        $file=DB::select('select * from arsip_file where pendaftaran_id = "'.$pendaftaran_id.'"');
        return view('pengelolaan_detail',['kirim' => $value, 'pendaftaran_id' => $pendaftaran_id, 'file' => $file]);
    }

    public function cekberkas($pendaftaran_id)
    {
        $value=DB::select('select DISTINCT a.pendaftaran_id, a.file, d.n_pemohon, f.n_perizinan, b.d_terima_berkas from arsip_file a inner join tmpermohonan b on a.pendaftaran_id = b.pendaftaran_id
inner join tmpemohon_tmpermohonan c on b.id = c.tmpermohonan_id 
inner join tmpemohon d on d.id = c.tmpemohon_id
inner join tmpermohonan_trperizinan e on e.tmpermohonan_id = b.id
inner join trperizinan f on f.id = e.trperizinan_id where a.pendaftaran_id = "'.$pendaftaran_id.'"');
        $file=DB::select('select * from arsip_file where pendaftaran_id = "'.$pendaftaran_id.'"');
        return view('cekberkas_detail',['kirim' => $value, 'pendaftaran_id' => $pendaftaran_id, 'file' => $file]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
