<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - Panel Admin</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core js') }} files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core js') }} files -->

    <!-- Theme js') }} files -->
    <!--<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/login_validation.js') }}"></script>
    <!-- /theme js') }} files -->

</head>

<body class="login-container login-cover">

    <!-- Page container -->
    <div class="page-container pb-20">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form with validation -->
                <form class="form-validate" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="form-control-feedback"> 
                            </div>
                        <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                            <h5 class="content-group"><b>ONE DATA</b><small class="display-block">Dinas Sosial</small></h5>
                        </div>
                        @if ($errors->has('email'))
                                <div class="form-group">
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('email') }}
                                    </div>
                                </div>
                                @endif
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input autocomplete="off" type="text" class="form-control" value="{{ old('email') }}" placeholder="Nama Pengguna" name="email" required="required">
                            
                        </div> 

                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" value="{{ old('email') }}" placeholder="Kata Sandi" name="password" required="required">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>  

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" name="remember">
                                        Ingat Saya
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="{{ url('/password/reset') }}">Lupa Kata Sandi?</a>
                                </div>
                            </div>
                        </div>  

                        <div class="form-group">
                            <button type="submit" class="btn bg-blue btn-block">Masuk <i class="icon-arrow-right14 position-right"></i></button>

                        </div>


                        <div class="content-divider text-muted form-group"><span>Dengan melanjutkan, anda telah setuju dengan <a href="#">Syarat &amp; Ketentuan</a> and <a href="#">Kebijakan Privasi</a></span></div>
                    </div>
                    </form>

 
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>
