@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Panel Pengelolaan data Arsip Perizinan</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Arsip</span> - Panel Pengelolaan data Arsip Perizinan</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			
			<!-- /main sidebar --> 

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Daftar Berkas dengan nomor Pendaftaran <?= $pendaftaran_id; ?></h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div> 
					<table class="table table-striped datatable-responsive">
						<thead>
							<tr> 
								<th>Nomor Pendaftaran</th>
								<th>Nama Pemohon</th>   
								<th>Jenis Izin</th>   
								<th>Tanggal</th>   
							</tr>
						</thead> 
						<tbody>
							<?php
							foreach($kirim as $data)
							{
								echo "
								<tr>
									<td>".$data['pendaftaran_id']."</td>
									<td>".$data['n_pemohon']."</td>
									<td>".$data['n_perizinan']."</td>
									<td>".$data['d_terima_berkas']."</td>
								</tr>
							";
							}
							?>
						</tbody>
					</table>
					<table id="example" class="table table-striped datatable-responsive">
						<thead>
							<tr> 
								<th>Berkas</th>
								<th>Aksi</th>  
							</tr>
						</thead> 
						<tbody>
							<?php
							foreach($file as $datafile)
							{
								echo "
								<tr>
									<td>".$datafile['file']."</td>
									<td><a href='".Config::get("global.base_url")."uploads/".$datafile['file']."' download><i class='icon-download' ></i> </a></td>
								</tr>
							";
							}
							?>
						</tbody>
						<tfoot>
							<tr>  
								<th>Berkas</th>
								<th>Aksi</th>   
							</tr>
						</tfoot> 
					</table>
				</div>
			</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/core/app.js"></script> 
	<script type="text/javascript">
		$(document).ready(function() {
    $('#example').DataTable( {
    	"order": [[ 1, "asc" ]], 
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Kata Kunci:</span> _INPUT_',
            searchPlaceholder: 'Ketik untuk mencari...',
            lengthMenu: '<span>Jumlah data per halaman:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }, 
    } ); 
} );
	</script>
</body>
</html>
