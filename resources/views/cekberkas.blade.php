<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ARSIP PERIZINAN KABUPATEN PADANG LAWAS UTARA</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core js') }} files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- /core js') }} files -->

	<!-- Theme js') }} files -->
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/login_validation.js') }}"></script>
	<!-- /theme js') }} files -->

</head>

<body class="login-container login-cover">

	<!-- Page container -->
	<div class="page-container pb-20">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Form with validation -->
				<form method="POST" action="form/pengecekan" class="form-validate">
					<div class="panel panel-body login-form">
						<div class="text-center">
							<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
							<h5 class="content-group"><b>ARSIP PERIZINAN</b><small class="display-block">DINAS PTSP </small></h5>
						</div>
						<input autocomplete='off' type="hidden" name="_token" value="{{ csrf_token() }}" class="form-control">

						<div class="form-group has-feedback has-feedback-left">
							<input autocomplete='off' type="text" class="form-control" placeholder="Nomor Pendaftaran" name="pendaftaran_id" required="required">
							<div class="form-control-feedback">
								<i class="icon-user text-muted"></i>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" class="btn bg-blue btn-block" value="Cek Berkas"> 
						</div>
						<span class="help-block text-center no-margin">Dengan melanjutkan, anda telah setuju dengan <a href="#">Syarat &amp; Ketentuan</a> and <a href="#">Kebijakan Privasi</a></span>
						@if (Session::has('gagal1'))
						<br>
						<br>
								<div class="alert alert-danger" role="alert">
									<p>{{ Session::get('gagal1')}}</p>
								</div>
								@endif
					</div>
				</form>

				<!-- /form with validation -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
