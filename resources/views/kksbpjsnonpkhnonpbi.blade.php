@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Rincian Detail Laporan Keluarga Miskin terdaftar peserta KKS & BPJS non PKH non PBI</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Laporan Umum</span> - Rincian Detail Laporan Keluarga Miskin terdaftar peserta KKS & BPJS non PKH non PBI</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Rincian Detail Laporan Keluarga Miskin terdaftar peserta KKS & BPJS non PKH non PBI</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div> 
					<table id="example" class="table table-striped datatable-responsive">
						<thead>
							<tr> 
								<th>NIK KK</th>
								<th>Nama Kepala Keluarga</th>   
								<th>Kecamatan</th>   
								<th>Kelurahan</th>   
								<th>Aksi</th>   
							</tr>
						</thead> 
						<tfoot>
							<tr>  
								<th>NIK KK</th>
								<th>Nama Kepala Keluarga</th>   
								<th>Kecamatan</th>   
								<th>Kelurahan</th>   
								<th>Aksi</th>   
							</tr>
						</tfoot> 
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/core/app.js"></script> 
	<script type="text/javascript">
		$(document).ready(function() {
    $('#example').DataTable( {
    	"order": [[ 1, "asc" ]],
        "processing": true,
        //"serverSide": true, 
        //"ajax": "<?= Config::get('global.domain');?>datatable_server/onedata_kkspkh.php",
         autoWidth: false, 
        responsive: true, 

        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Kata Kunci:</span> _INPUT_',
            searchPlaceholder: 'Ketik untuk mencari...',
            lengthMenu: '<span>Jumlah data per halaman:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }, 
    } ); 
} );
	</script>
</body>
</html>
