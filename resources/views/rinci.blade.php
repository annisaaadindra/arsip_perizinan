@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Rincian Detail Laporan Keluarga Miskin</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Laporan Umum</span> - Rincian Detail Laporan Keluarga Miskin</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Rincian Detail Laporan Keluarga Miskin</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div> 
					<table class="table table-striped datatable-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Aksi</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Laporan rincian anggota keluarga miskin terdaftar BDT non PKH dan KKS</td>
								<td><a href="{{ url('bdtnonpkhkks') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr class="success">
								<td>2</td>
								<td>Laporan keluarga miskin terdaftar BDT peserta KKS non PKH</td>
								<td><a href="{{ url('kksnonpkh') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr class="success">
								<td>3</td>
								<td>Laporan keluarga miskin terdaftar BDT peserta KKS & PKH</td>
								<td><a href="{{ url('kkspkh') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>4</td>
								<td>Laporan keluarga miskin terdaftar BDT dan BPJS non KKS & non PKH & non PBI</td>
								<td><a href="{{ url('bdtbpjsnonkksnonpkhnonpbi') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>5</td>
								<td>Laporan keluarga miskin terdaftar BPJS non PBI</td>
								<td><a href="{{ url('bpjsnonpbi') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>6</td>
								<td>Laporan keluarga miskin terdaftar peserta KKS & BPJS non PKH non PBI</td>
								<td><a href="{{ url('kksbpjsnonpkhnonpbi') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>7</td>
								<td>Laporan keluarga miskin terdaftar peserta PKH, KKS & BPJS non PBI</td>
								<td><a href="{{ url('pkhkksbpjsnonpbi') }}"><i class="icon-list"></i></a></td> 
							</tr> 
							<tr>
								<td>8</td>
								<td>Laporan keluarga miskin terdaftar BDT & PBI non KKS & PKH</td>
								<td><a href="{{ url('bdtpbinonkkspkh') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr class="success">
								<td>9</td>
								<td>Laporan keluarga miskin peserta KKS, PBI non PKH</td>
								<td><a href="{{ url('kkspbi_nonpkh') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>10</td>
								<td>Laporan keluarga miskin usulan penerima PBI/BPJS</td>
								<td><a href="{{ url('usulanpbibpjs') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>11</td>
								<td>Laporan keluarga miskin usulan penerima PBI/BPJS terdaftar BDT non PKH & KKS</td>
								<td><a href="{{ url('pbibpjsbdtnonpkhkks') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>12</td>
								<td>Laporan keluarga miskin usulan penerima PBI/BPJS terdaftar BDT peserta KKS non PKH</td>
								<td><a href="{{ url('pbibpjsbdtkksnonpkh') }}"><i class="icon-list"></i></a></td> 
							</tr>
							<tr>
								<td>13</td>
								<td>Laporan keluarga miskin usulan penerima PBI/BPJS terdaftar BDT peserta KKS & PKH</td>
								<td><a href="{{ url('pbibpjsbdtkkspkh') }}"><i class="icon-list"></i></a></td> 
							</tr> 
						</tbody>
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?= Config::get('global.base_url');?>assets/js/pages/datatables_responsive.js"></script>

</body>
</html>
