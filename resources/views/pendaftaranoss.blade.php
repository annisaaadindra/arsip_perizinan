@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><i class="icon-home2 position-left"></i> Beranda</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Input Data</span> - Data Berkas Perizinan 

				</h4>
			</div>

			<div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistik</span></a>
				</div>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Main charts -->
				<div class="content"> 
 
					<!-- /checkboxes -->

 
					<!-- /radio buttons -->


					<!-- Colors --> 
    				<!-- /colors -->


					<!-- Switchery toggles -->
					<div style="display:none;" class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Switchery toggles</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Default switchers</h6>
										<p class="content-group">You can add as many switches as you like, as long as their corresponding checkboxes have the same class. Select them and make new instance of the Switchery class for every of them.</p>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Checked switch
											</label>
										</div>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery">
												Unchecked switch
											</label>
										</div>

										<div class="checkbox checkbox-switchery disabled">
											<label>
												<input type="checkbox" class="switchery" checked="checked" disabled="disabled">
												Checked disabled
											</label>
										</div>

										<div class="checkbox checkbox-switchery disabled">
											<label>
												<input type="checkbox" class="switchery" disabled="disabled">
												Unchecked disabled
											</label>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Switcher colors</h6>
										<p class="content-group">You can change the default color of the switch to fit your design perfectly. According to the color system, any of its color can be applied to the switchery. Custom colors are also supported.</p>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery-primary" checked="checked">
												Switch in <span class="text-semibold">primary</span> context
											</label>
										</div>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery-danger" checked="checked">
												Switch in <span class="text-semibold">danger</span> context
											</label>
										</div>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery-info" checked="checked">
												Switch in <span class="text-semibold">info</span> context
											</label>
										</div>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery-warning" checked="checked">
												Switch in <span class="text-semibold">warning</span> context
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Single label</h6>
										<p class="content-group">You can choose one of 4 main Switch sizes - large (29px height), default (25px height), small (21px height) and mini (17px height). Just add proper class to the <code>.checkbox</code> wrapper.</p>

										<div class="checkbox checkbox-switchery switchery-lg">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Large size
											</label>
										</div>

										<div class="checkbox checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Default size
											</label>
										</div>

										<div class="checkbox checkbox-switchery switchery-sm">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Small size
											</label>
										</div>

										<div class="checkbox checkbox-switchery switchery-xs">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Mini size
											</label>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Multiple labels</h6>
										<p class="content-group">Switchery can be used with single label or with multiple labels. To use, add <code>.switchery-double</code> class to the container. All sizing options are available for this switchery type as well.</p>

										<div class="checkbox checkbox-switchery switchery-lg switchery-double">
											<label>
												Option 1
												<input type="checkbox" class="switchery" checked="checked">
												Option 2
											</label>
										</div>

										<div class="checkbox checkbox-switchery switchery-double">
											<label>
												Option 1
												<input type="checkbox" class="switchery" checked="checked">
												Option 2
											</label>
										</div>

										<div class="checkbox checkbox-switchery switchery-sm switchery-double">
											<label>
												Option 1
												<input type="checkbox" class="switchery" checked="checked">
												Option 2
											</label>
										</div>

										<div class="checkbox checkbox-switchery switchery-xs switchery-double">
											<label>
												Option 1
												<input type="checkbox" class="switchery" checked="checked">
												Option 2
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Right alignment</h6>
										<p class="content-group">Default switchery position is left. Use <code>.checkbox-right</code> class to change switchery position to right. This class sets correct side padding for label and changes main position.</p>

										<div class="checkbox checkbox-right checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery" checked="checked">
												Checked switch
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery">
											<label>
												<input type="checkbox" class="switchery">
												Unchecked switch
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery disabled">
											<label>
												<input type="checkbox" class="switchery" checked="checked" disabled="disabled">
												Checked disabled
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery disabled">
											<label>
												<input type="checkbox" class="switchery" disabled="disabled">
												Unchecked disabled
											</label>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Sticked to the right</h6>
										<p class="content-group">Sometimes it's very useful to have switches on the right side of the container. Just add <code>.display-block</code> class to the <code>&lt;label></code> for the very right side switcher alignment.</p>

										<div class="checkbox checkbox-right checkbox-switchery">
											<label class="display-block">
												<input type="checkbox" class="switchery" checked="checked">
												Checked switch
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery">
											<label class="display-block">
												<input type="checkbox" class="switchery">
												Unchecked switch
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery disabled">
											<label class="display-block">
												<input type="checkbox" class="switchery" checked="checked" disabled="disabled">
												Checked disabled
											</label>
										</div>

										<div class="checkbox checkbox-right checkbox-switchery disabled">
											<label class="display-block">
												<input type="checkbox" class="switchery" disabled="disabled">
												Unchecked disabled
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /switchery toggles -->


					<!-- Bootstrap switch -->
					 
					<!-- Basic setup -->
		            <div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">Input Data Pendaftaran</h6>

							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

	                	<form class="stepy-basic" method="POST" action="form/pendataanoss" enctype="multipart/form-data">
	                		{{ csrf_field() }}
							<fieldset title="1">
								@if (Session::has('berhasil'))
								<div class="alert alert-success" role="alert">
									<p>{{ Session::get('berhasil')}}</p>
								</div>
								@endif
								@if (Session::has('gagal'))
								<div class="alert alert-danger" role="alert">
									<p>{{ Session::get('gagal')}}</p>
								</div>
								@endif
								@if (Session::has('gagal1'))
								<div class="alert alert-danger" role="alert">
									<p>{{ Session::get('gagal1')}}</p>
								</div>
								@endif
								<legend class="text-semibold">Data Pendaftaran</legend>

								<div class="row">
									<!--<div class="col-md-6">
										<div class="form-group">
											<label>Title:</label>
											<select name="1" data-placeholder="Select position" class="select"> 
													<option value="1">Tn.</option>
													<option value="2">Ny.</option>  
											</select>
										</div>
									</div>-->
									<div class="col-md-6">
										<div class="form-group">
											<label>No Pendaftaran OSS<small><i style="color: red"> (Harus sama dengan yang ada di OSS) </i></small>:</label>
											<input autocomplete='off' type="text" name="no_pendaftaran" class="form-control" required="">
											<input autocomplete='off' type="hidden" name="_token" value="{{ csrf_token() }}" class="form-control">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Nama<small></small>:</label>
											<input autocomplete='off' type="text" name="nama" class="form-control" required="">
											<input autocomplete='off' type="hidden" name="_token" value="{{ csrf_token() }}" class="form-control">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>File-file Lampiran <small><i style="color: red">(Boleh Upload Lebih dari 1 Files) </i></small>:</label>
											<input autocomplete='off' type="file" name="images[]" class="form-control" multiple="">
										</div>
									</div>
								</div>

							</fieldset> 
							<input type="submit" class="btn btn-primary stepy-finish" value="Simpan" name="submit-reservasi">
						</form>
		            </div>
		            <!-- /wizard with callbacks -->
		            <script type="text/javascript">
		            		var count = 2;

		            	$('#tambah').click(function(){
		            		$('#tambahan').append('<hr><b>Data Anggota Keluarga '+(count++)+'</b><br><br><div class="row"><div class="col-md-4"><div class="form-group"><label>Nama:</label><input autocomplete="off" type="text" name="2" class="input-sm form-control"></div></div><div class="col-md-3"> <div class="form-group"> <label>NIK:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>Jenis Kelamin:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label>Tempat Lahir:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label>Tanggal Lahir:</label> <input autocomplete="off" type="date" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>Agama:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>Pendidikan Terakhir:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>Agama:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label>Jenis Pekerjaan:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>Kewarganegaraan:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>No Passpor:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label>No KITAS/KITAP:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label>Nama Ayah:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label>Nama Ibu:</label> <input autocomplete="off" type="text" name="3" class="input-sm form-control"> </div> </div> </div>');
		            	});
		            </script>
		            <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/wizards/stepy.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/libraries/jasny_bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/pages/wizard_stepy.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>

					<!-- Footer --> 
					
					<!-- /footer -->

				</div>
				<!-- /main charts -->

 
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')

	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js') }}"></script>

</body>
</html>
